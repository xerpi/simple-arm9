#include "lib.h"
#include "3ds.h"


uint64_t GetSystemTick(void)
{
    register unsigned long lo64 asm ("r0");
    register unsigned long hi64 asm ("r1");
    asm volatile ( "SVC 0x28" : "=r"(lo64), "=r"(hi64) );
    return ((uint64_t)hi64<<32) | (uint64_t)lo64;
}
