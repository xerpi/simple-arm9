#include "3dstypes.h"
#include "draw.h"
#include "lib.h"
#include "3ds.h"
#include "pad.h"
#include "utils.h"
#include "utils_asm.h"
#include "irq.h"

const char *size_table[] = {
    "  4KB", "  8KB", " 16KB", " 32KB", "  64KB", "128KB", "256KB", "512KB",
    "  1MB", "  2MB", "  4MB", "  8MB", "  16MB", " 32MB", " 64MB", "128MB", "256MB", "512MB",
    "  4GB"
};

#define dump_area(n) \
    do { \
        u32 reg; \
        asm("MRC p15, 0, %0, c6, c"#n", 0\n" \
            : "=r" (reg) : :); \
        DEBUG("area "#n": 0x%X  size %s %s", \
            (reg>>12)&0xFFFFF, \
            size_table[((reg>>1)&0b111111)-0b01011], \
            (reg&0b1) ? "enabled" : "disabled"); \
    } while (0)

void dump_cp15_regions()
{
    dump_area(0);
    dump_area(1);
    dump_area(2);
    dump_area(3);
    dump_area(4);
    dump_area(5);
    dump_area(6);
    dump_area(7);
}

#define PRIV 0
#define USER 1

const char *perm_table_str[][2] = {
    {"No access", "No access"},
    {"RW", "No access"},
    {"RW", "R access"},
    {"RW", "RW"},
    {"UNP", "UNP"},
    {"R access", "No access"},
    {"R access", "R access"},
    {"UNP", "UNP"}
};

const char *perm_table_shortstr[][2] = {
    {"--", "--"},
    {"RW", "--"},
    {"RW", "R-"},
    {"RW", "RW"},
    {"??", "??"},
    {"R-", "--"},
    {"R-", "R-"},
    {"??", "??"}
};

void dump_cp15_write_data_perm()
{
    u32 reg, bits, i;
    asm("MRC p15, 0, %0, c5, c0, 2\n"
        : "=r" (reg) : :);
        
    //Set permissions!
    //reg |= (0b0011<<20);
    //asm("MCR p15, 0, %0, c5, c0, 2\n"::"r"(reg):);
        
        
    asm("MRC p15, 0, %0, c5, c0, 2\n"
        : "=r" (reg) : :);
    
    DEBUG("Write Data permissions:");
    for (i = 0; i < 8; i++) {
        bits = (reg>>(i*4))&0xF;
        DEBUG("   area %d: Priv: %s  User: %s",
            i,
            perm_table_str[bits][PRIV],
            perm_table_str[bits][USER]);  
    }
}


void dump_cp15_permissions()
{
    u32 data_perm = cp15_get_data_access_reg();
    u32 instr_perm = cp15_get_instr_access_reg();
    
    int i;
    for (i = 0; i < 8; i++) {
        u32 reg = cp15_get_protection_reg(i);
        
        u32 base = (reg>>12)&0xFFFFF;
        u32 size_idx = ((reg>>1)&0b111111)-0b01011;
        u32 size = 0x1000<<size_idx;
        u32 enabled = (reg&0b1);
        
        u32 start_address = base<<12;
        u32 end_address = start_address | size;
        
        u32 d_perm = (data_perm>>(i*4))&0xF;
        u32 i_perm = (instr_perm>>(i*4))&0xF;
        
        DEBUG("Area %i: 0x%08X-0x%08X, %s %s",
            i,
            start_address,
            end_address,
            size_table[size_idx],
            enabled ? "(enabled)" : "disabled");
            
        DEBUG("    Permissions: DATA: %s%s, INSN: %s%s",
            perm_table_shortstr[d_perm][PRIV],
            perm_table_shortstr[d_perm][USER],
            perm_table_shortstr[i_perm][PRIV],
            perm_table_shortstr[i_perm][USER]);
    }
}

extern void green_screen();
extern void *green_screen_end;
extern void greenitize();
extern void *greenitize_end;

int main()
{
    ClearScreen(0x0);
    
    int y = 10;
    
    #define ERR(args...) font_draw_stringf(10, y+=10, RED, args)
    #define DBG(args...) font_draw_stringf(10, y+=10, WHITE, args)
    
    DBG("MPU Hoaxor");
    DBG("");
    DBG("");
    DBG("");
    
    u32 reg = cp15_get_control_reg();
    
    DBG("Before Control register: 0x%08X", reg);
    DBG("MPU is %s", (reg & 0b1) ? "enabled" : "disabled");
    
    reg &= ~1;
    
    ERR("I'll set control register to: 0x%08X", reg);
    ERR("/!\\ DANGER /!\\");
    ERR("HACKING MPU MUAHAHA");
    
    cp15_set_control_reg(reg);
    
    reg = cp15_get_control_reg();
    DBG("After control register: 0x%08X", reg);
    DBG("MPU is %s", (reg & 0b1) ? "enabled" : "disabled");
    
    
    #define ADDRESS ((void *)0x20000004)
    l_memcpy(ADDRESS, (void*)&greenitize, (u32)&greenitize_end-(u32)&greenitize);
    
    l_memcpy((void *)0x20001000, (void*)&green_screen, (u32)&green_screen_end-(u32)&green_screen);
    
    DBG("hacking IRQ");
    
    
    u32 old = *(u32*)0x1FFF4018;
    *(u32*)0x20000000 = *(u32*)0x1FFF4018;
    
    *(u32*)0x1FFF4018 = 0x42F2A0E3;//0xe3a0f202;
    //E3A0F242
    
    DBG("hacked!");
    
    while (1) {
        u32 val = *(u32*)0x1FFF4018;
        if (val != old) {
            DBG("ARM11 CONTROL");
            break;
        }
    }
    
    return 0;
}







