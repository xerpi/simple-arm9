#include "draw.h"
#include "3ds.h"

int console_y = 1;
extern const u8 msx_font[];

void ClearScreen(u32 color)
{
    int i;
    for (i = 0; i < FB_TOP_SIZE; i+=4) {
        *(int *)(FB_TOP_LEFT1 + i) = color;
    }
}

void console_reset()
{
    ClearScreen(0x0);
    console_y = 1;
}

void font_draw_char(int x, int y, u32 color, char c)
{
    if(c == ' ') return;
    u8 *font = (u8*)(msx_font + c * 8);
    int i, j;
    for (i = 0; i < 8; ++i) {
        for (j = 0; j < 8; ++j) {
            if ((*font & (128 >> j))) draw_plot(x+j, y+i, color);
        }
        ++font;
    }
}
void font_draw_string(int x, int y, u32 color, const char *string)
{
    if(string == NULL) return;
    int startx = x;
    const char *s = string;
    while(*s) {
        if(*s == '\n') {
            x = startx;
            y+=8;
        } else if(*s == '\t') {
            x+=8*4;
        } else {
            font_draw_char(x, y, color, *s);
            x+=8;
        }
        ++s;
    }
}

void font_draw_stringf(int x, int y, u32 color, const char *s, ...)
{
    char buf[128];
    char *dest = buf;
    va_list va;
    va_start(va, s);
    tfp_format(&dest, putcp, (char*)s, va);
    putcp(&dest, 0);
    va_end(va);
    font_draw_string(x, y, color, buf);
}

void draw_fillrect(int x, int y, int w, int h, u32 color)
{
    int i, j;
    for (i = 0; i < w; ++i) {
        for (j = 0; j < h; ++j) {
            draw_plot(x+i, y+j, color);
        }
    }
}

