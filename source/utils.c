#include "utils.h"
#include "draw.h"
#include "pad.h"
#include "3ds.h"

/*#define diff_ticks(tick0,tick1)		(((u64)(tick1)<(u64)(tick0))?((u64)-1-(u64)(tick0)+(u64)(tick1)):((u64)(tick1)-(u64)(tick0)))
void usleep(uint64_t ticks)
{
    uint64_t start = GetSystemTick();
    while (1)
    {
        uint64_t end = GetSystemTick();
        if (diff_ticks(start,end) >= ticks)
            break;
    }
}
*/

void nsleep(uint64_t ns)
{
    uint64_t ticks = ns;
    uint64_t start = GetSystemTick();
    while ((GetSystemTick() - start) < ticks);
}


void usleep(uint64_t us)
{
    uint64_t ticks = us * TICKS_PER_USEC;
    uint64_t start = GetSystemTick();
    while ((GetSystemTick() - start) < ticks);
}

void msleep(uint64_t ms)
{
    uint64_t ticks = ms * TICKS_PER_MSEC;
    uint64_t start = GetSystemTick();
    while ((GetSystemTick() - start) < ticks);  
}

void sleep(uint64_t s)
{
    uint64_t ticks = s * TICKS_PER_SEC;
    uint64_t start = GetSystemTick();
    while ((GetSystemTick() - start) < ticks); 
}


