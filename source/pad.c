#include "pad.h"

int CheckButtons(uint32_t mask)
{
	uint32_t value = ~mask & PAD_NONE;
	return (*IO_PAD == value);
}

void WaitButtonsPressed(uint32_t mask)
{
	uint32_t value = ~mask & PAD_NONE;
	while (*IO_PAD != value) ;
}

