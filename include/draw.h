#ifndef _DRAW_H_
#define _DRAW_H_

#include "3dstypes.h"
#include "3ds.h"

#define RED    0xFF0000
#define GREEN  0x00FF00
#define BLUE   0x0000FF
#define BLACK  0x000000
#define WHITE  0xFFFFFF

static inline void draw_plot(int x, int y, u32 color)
{
    unsigned char *p = (unsigned char *)(FB_TOP_LEFT1 + (SCREEN_TOP_H-y-1)*3 +x*3*SCREEN_TOP_H);
    p[0] = color & 0xFF;
    p[1] = (color>>8) & 0xFF;
    p[2] = (color>>16) & 0xFF;
}

void draw_fillrect(int x, int y, int w, int h, u32 color);
void ClearScreen(u32 color);
void console_reset();

void font_draw_char(int x, int y, u32 color, char c);
void font_draw_string(int x, int y, u32 color, const char *string);
void font_draw_stringf(int x, int y, u32 color, const char *s, ...);

#define FlushFB() FlushProcessDataCache(0xFFFF8001, (void*)FB_TOP_LEFT1, FB_TOP_SIZE);

extern int console_y, console_x;

#define ERROR(args...) font_draw_stringf(10, console_y+=10, RED, args)
#define DEBUG(args...) font_draw_stringf(10, console_y+=10, WHITE, args)



#endif
