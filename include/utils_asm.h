#ifndef UTILS_ASM_H
#define UTILS_ASM_H

#include "3dstypes.h"

extern unsigned long cp15_get_control_reg();
extern void cp15_set_control_reg(unsigned long n);

extern unsigned long cp15_get_protection_reg(u32 i);

extern unsigned long cp15_get_data_access_reg();
extern unsigned long cp15_get_instr_access_reg();

extern void cp15_set_data_access_reg(unsigned long n);
extern void cp15_set_instr_access_reg(unsigned long n);

#endif

