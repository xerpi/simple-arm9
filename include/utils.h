#ifndef UTILS_H
#define UTILS_H

#include "3dstypes.h"

#define ROUND_UP(x,y)  (x=(x+(y-1))&~(x-1))

#define TICKS_PER_SEC 0x80C0000
#define TICKS_PER_MSEC (TICKS_PER_SEC/1000)
#define TICKS_PER_USEC (TICKS_PER_MSEC/1000)

void nsleep(uint64_t ns);
void usleep(uint64_t us);
void msleep(uint64_t ms);
void sleep(uint64_t s);

#endif

