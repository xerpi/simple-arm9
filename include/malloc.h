/*
 * Helper for use with the PSP Software Development Kit - http://www.pspdev.org
 * -----------------------------------------------------------------------
 * Licensed as 'free to use and modify as long as credited appropriately'
 *
 * vram.h - Standard C high performance VRAM allocation routines.
 *
 * Copyright (c) 2007 Alexander Berl 'Raphael' <raphael@fx-world.org>
 * http://wordpress.fx-world.org
 *
 * Ported to the 3DS by xerpi
 */

#ifndef malloc_h__
#define malloc_h__

#include "3dstypes.h"

#ifdef __cplusplus
extern "C" {
#endif

void* malloc( size_t size );
void free( void* ptr );
size_t memavail();
size_t memlargestblock();


#ifdef _DEBUG
// Debug printf (to stdout) a trace of the current Memblocks
void __memwalk();
#endif

#ifdef __cplusplus
}
#endif

#endif  // ifdef vram_h__
