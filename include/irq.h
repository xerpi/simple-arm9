#ifndef IRQ_H
#define IRQ_H

#include "3dstypes.h"

#define REG_IE ((vu32*)0x10001000)
#define REG_IF ((vu32*)0x10001004)

static inline vu32 disableInterrupts()
{
    int oldIE = *REG_IE;
    *REG_IE = 0;
    return oldIE;
}
static inline void enableInterrupts(vu32 oldIE)
{
    *REG_IE = oldIE;
}

#endif

