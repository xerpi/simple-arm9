/* Grabbed from HBL (https://valentine-hbl.googlecode.com)*/


#ifndef _LIBC_H_
#define _LIBC_H_

#include "3dstypes.h"

// Sets a memory region to a specific value
void * l_memset(void *ptr, int c, size_t size);

// Copies one memory buffer into another
void * l_memcpy(void *out, const void *in, int size);

//Scan s for the character.  When this loop is finished,
//    s will either point to the end of the string or the
//    character we were looking for
char *(strchr)(const char *s, int c);

// Returns string length
int l_strlen(const char *text);

// Copies string src into dest
char *l_strcpy(char *dest, const char *src);

void l_wstr_to_str(const wchar_t *in, char *out);
void l_numu64tostr(uint64_t n, char *out);

// Searches for s1 string in memory
// Returns pointer to string
char* l_memfindsz(const char* s1, char* start, unsigned int size);

// Searches for 32-bit value on memory
// Returns pointer to value
u32* l_memfindu32(const u32 val, u32* start, unsigned int size);

// Compares s1 with s2, returns 0 if both equal
int l_strcmp(const char *s1, const char *s2);

// Concatenates string s + append
char *l_strcat(char *s, const char *append);

// Compares only "count" chars from strings
int l_strncmp(const char *s1, const char *s2, size_t count);

char * l_strrchr(const char *cp, int ch);


uint32_t l_wstrlen(const wchar_t *str);
wchar_t *l_wstrcat(wchar_t *destination, const wchar_t *source);

#include <stdarg.h>

void init_printf(void *putp, void (*putf) (void *, char));
void putcp(void *p, char c);
void tfp_printf(char *fmt, ...);
void tfp_sprintf(char *s, char *fmt, ...);

void tfp_format(void *putp, void (*putf) (void *, char), char *fmt, va_list va);

#define printf tfp_printf
#define sprintf tfp_sprintf

#endif
