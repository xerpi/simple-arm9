#ifndef _3DS_H_
#define _3DS_H_

#include "3dstypes.h"
#include "lib.h"

#define FCRAM     (0x20000000)
#define FCRAM_END (0x28000000)
#define SCREEN_TOP_W  (400)
#define SCREEN_BOT_W  (340)
#define SCREEN_TOP_H  (240)
#define SCREEN_BOT_H  (240)
#define FB_TOP_LEFT1   (0x20184E60)

#define FB_TOP_LEFT1  (0x20184E60)
#define FB_TOP_LEFT2  (0x201CB370)
#define FB_TOP_RIGHT1 (0x20282160)
#define FB_TOP_RIGHT2 (0x202C8670)
#define FB_BOT_1      (0x202118E0)
#define FB_BOT_2      (0x20249CF0)

#define FB_TOP_SIZE   (0x46500)
#define FB_BOT_SIZE   (0x3BC40)


uint64_t GetSystemTick(void);
static inline void ExitProcess(void){asm volatile ( "SVC 0x3" );}
static inline int FlushProcessDataCache(uint32_t process, void *addr, uint32_t size)
{
    register long r0 asm ("r0") = process;
    register void *r1 asm ("r1") = addr;
    register long r2 asm ("r2") = size;
    asm volatile ( "SVC 0x54" : "=r"(r0) : "r"(r0), "r"(r1), "r"(r2) );
    return r0;
}
static inline int InvalidateProcessDataCache(uint32_t process, void *addr, uint32_t size)
{
    register long r0 asm ("r0") = process;
    register void *r1 asm ("r1") = addr;
    register long r2 asm ("r2") = size;
    asm volatile ( "SVC 0x52" : "=r"(r0) : "r"(r0), "r"(r1), "r"(r2) );
    return r0;
}
static inline int ConnectToPort(uint32_t *handle, const char* portName)
{
    register uint32_t *r0 asm ("r0") = handle;
    register const char* r1 asm ("r1") = portName;
    asm volatile ( "SVC 0x2D" : "=r"(r0), "=r"(r1) : "r"(r0), "r"(r1) );
    *handle = (uint32_t)r1;
    return (uint32_t)r0;
}
static inline int CloseHandle(uint32_t handle)
{
    register uint32_t r0 asm ("r0") = handle;
    asm volatile ( "SVC 0x23" : "=r"(r0) : "r"(r0) );
    return r0;
}

static inline int SendSyncRequest(uint32_t handle)
{
    register long r0 asm ("r0") = handle;
    asm volatile ( "SVC 0x32" : "=r"(r0) : "r"(r0) );
    return r0;
}
static inline void SetCommandHeader(uint32_t id, uint32_t header)
{
    *(uint32_t *)(id + 0x80) = header;
}
static inline void SetCommandData(uint32_t id, void *addr, uint32_t size)
{
    l_memcpy((void*)(id + 0x84), addr, size);
}
static inline uint32_t* GetResponseAddr(uint32_t id)
{
    return (uint32_t*)(id + 4);
}
static inline void SendCommand(uint32_t handle, uint32_t process_id, uint32_t cmd_header, void *cmd_addr, uint32_t cmd_size)
{
    SetCommandHeader(process_id, cmd_header);
    if (cmd_addr && cmd_size) {
        SetCommandData(process_id, cmd_addr, cmd_size);
    }
    SendSyncRequest(handle);
}

#endif

