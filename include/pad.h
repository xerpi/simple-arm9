#ifndef PAD_H
#define PAD_H

#include "3dstypes.h"

#define IO_PAD ((vu32*)0x10146000)

#define PAD_NONE   (0xFFF)
#define PAD_A      (1<<0)
#define PAD_B      (1<<1)
#define PAD_SELECT (1<<2)
#define PAD_START  (1<<3)
#define PAD_RIGHT  (1<<4)
#define PAD_LEFT   (1<<5)
#define PAD_UP     (1<<6)
#define PAD_DOWN   (1<<7)
#define PAD_R      (1<<8)
#define PAD_L      (1<<9)
#define PAD_X      (1<<10)
#define PAD_Y      (1<<11)

int CheckButtons(uint32_t mask);
void WaitButtonsPressed(uint32_t mask);

#endif

