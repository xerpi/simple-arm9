OUTPUT_FORMAT("elf32-littlearm", "elf32-littlearm", "elf32-littlearm")
OUTPUT_ARCH(arm)

/*0x080C3EE0*/
ENTRY(_start)

SECTIONS
{
  . = 0x080C3EE0;
  entry_addr = .;
  .text.start : { *(.text.start ) }
  .text       : { *(.text) }
  .rodata     : { *(.rodata) *(.rodata*) }
  .data       : { *(.data) }
  .bss        : { *(.bss) }
  . = ALIGN(8);
  .stack : {
    stack_start = .;
    . += 0x1000;
    stack_end = .;
  }
}
