TARGET := simple-arm9

SOURCES = source
INCLUDES = include

C_FILES := $(foreach dir,$(SOURCES),$(wildcard $(dir)/*.c))
S_FILES := $(foreach dir,$(SOURCES),$(wildcard $(dir)/*.S))
OBJS    := $(C_FILES:.c=.o) $(S_FILES:.S=.o)

LIBS =

PREFIX=arm-none-eabi
CC=$(PREFIX)-gcc
LD=$(PREFIX)-ld
STRIP=$(PREFIX)-strip
OBJCOPY=$(PREFIX)-objcopy

ARCH    = -mthumb-interwork  -mcpu=arm946e-s -march=armv5te -mlittle-endian
ASFLAGS = $(ARCH) -fno-builtin -nostartfiles
CFLAGS  = -Wall $(ARCH)
CFLAGS  += $(foreach dir,$(INCLUDES),-I$(dir))

all: Launcher.dat

Launcher.dat: $(OBJS)
	$(LD) -T linker.x $(OBJS) $(LIBS) -o $(TARGET).elf
	$(STRIP) -s $(TARGET).elf
	$(OBJCOPY) -O binary $(TARGET).elf $(TARGET).bin
	python tools/3dsploit.py $(TARGET).bin

%.o: %.c
	$(CC) -O1 -mthumb $(CFLAGS) -c -o $@ $<

%.o: %.S
	$(PREFIX)-g++ -marm $(ASFLAGS) -g -c $< -o $@

source/main.o: source/main.c
	$(CC) -O1 -marm $(CFLAGS) -c -o $@ $<

source/lib.o: source/lib.c
	$(CC) -O2 -marm $(CFLAGS) -c -o $@ $<
	

copy: Launcher.dat
	cp Launcher.dat /home/$(USER)/Dropbox/3dsarm11

clean:
	rm -r -f $(OBJS) $(TARGET).elf $(TARGET).bin Launcher.dat
